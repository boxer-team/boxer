#!/usr/bin/perl

use v5.14;
use utf8;

use Test::More;
use Test::Code::TidyAll;

use strictures 2;
no warnings "experimental::signatures";

tidyall_ok();

done_testing();
