#!/usr/bin/perl

use v5.20;
use utf8;
use feature 'signatures';

use Test::More;

use Path::Tiny;
use Test::Synopsis;

use strictures 2;
no warnings "experimental::signatures";

# resolve module list ourselves: MANIFEST is missing during development
my $modules = path('lib')->visit(
	sub ( $path, $state ) {
		return if $path->is_dir;
		return if $path !~ /\.pm$/;
		$state->{$path} = 1;
	},
	{ recurse => 1 }
);

synopsis_ok( keys %{$modules} );

done_testing();
